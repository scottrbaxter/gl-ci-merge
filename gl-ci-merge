#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

DEFAULT_FILE=".gitlab-ci.yml"

arg_vars(){
    # Set args
    case "$1" in
        -f|--file )
            file_path="$2"
            ;;
        -h|--help )
            help_menu
            exit
            ;;
        -p|--project )
            ci_project_path="$2"
            ;;
        -r|--ref )
            ref="$2"
            ;;
        -u|--url )
            url="$2"
            ;;
        -t|--token )
            gitlab_api_token="$2"
            ;;
        -v|--verbose )
            verbose=true
            ;;
    esac
}

encode_args(){
    encoded_file_path="$(url_encode "${file_path}")"
    encoded_project_path="$(url_encode "${ci_project_path}")"
}

gather_missing_args(){
    [[ "${ref}" ]] || ref="$(get_ref)"
    [[ "${file_path}" ]] || file_path="$(get_file_path)"
    [[ "${gitlab_api_token}" ]] || gitlab_api_token="$(get_gitlab_api_token)"
    if [[ -z "${ci_project_path}" ]] || [[ -z "${url}" ]]; then
        remote_origin_url="$(git config --get remote.origin.url)"
        [[ "${ci_project_path}" ]] ||  ci_project_path="$(get_ci_project_path)"
        [[ "${url}" ]] || url="$(get_url)"
        if [[ -z "${ci_project_path}" ]]; then
            echo "[ERROR] no project path available"
            exit 1
        elif [[ -z "${ref}" ]]; then
            echo "[ERROR] no git ref available"
            exit 1
        fi
    fi
}

get_ci_project_path(){
    if [[ "$remote_origin_url" =~ ^http ]]; then
        project_path="$(git config --get remote.origin.url \
            | awk -F/ '{print $3"/"$NF}' | sed 's/\.git$//')"
    elif [[ "$remote_origin_url" =~ ^git@ ]]; then
        project_path="$(git config --get remote.origin.url \
            | sed 's/^.*://;s/.git$//')"
    fi
    echo "${project_path}"
}

get_gitlab_api_token(){
    [[ "$GITLAB_TOKEN" ]] && gitlab_api_token="${GITLAB_TOKEN}"
    echo "$gitlab_api_token"
}

get_file_path(){
    echo "${file_path:-$DEFAULT_FILE}"
}

get_ref(){
    git branch --show-current
}

get_url(){
    if [[ "$remote_origin_url" =~ ^http ]]; then
        url="$(git config --get remote.origin.url \
            | awk -F/ '{print $1"//"$3}')"
    elif [[ "$remote_origin_url" =~ ^git@ ]]; then
        url="https://$(git config --get remote.origin.url \
            | sed 's/^.*@//;s/:.*$//')"
    fi
    echo "${url}"
}

help_menu(){
    echo -e "Help Menu
    -f|--file       File or template to merge, defaults to \".gitlab-ci.yml\"
    -h|--help       Display (this) help menu
    -r|--ref        git tag or branch
    -p|--project    project path e.g. \"group/subgroup/project\"
    -u|--url        API domain e.g. \"gitlab.com\"
    -t|--token      Gitlab api token or GITLAB_TOKEN env var must be specified
    -v|--verbose    pass this arg to print 'Merged YAML' banner
\\nNote: The --project and --url args are obtained automatically from git config
      in the current working directory if either are not specified.\\n"
}

initalize_vars(){
    ref=''
    file_path=''
    gitlab_api_token=''
    ci_project_path=''
    url=''
    verbose=''
}

main(){
    initalize_vars
    parse_args "${@}"

    # if local file does not exist, attempt to
    # pull from upstream on specified ref (branch)
    if [[ -f "${file_path}" ]]; then
        curl_data="$(yq -c '.' "${file_path}")"
    else
        # can obtain the file upstream without existing locally
        curl_data="$(curl --silent --location \
            --request GET "$url/projects/$encoded_project_path/repository/files/$encoded_file_path?ref=$ref" \
            --header "Authorization: Bearer $gitlab_api_token" \
            | yq -r '.content |= @base64d | .content')"
    fi

    response="$(jq --null-input --arg yaml "$curl_data" '.content=$yaml' \
        | curl --location --silent "$url/projects/$encoded_project_path/ci/lint?include_merged_yaml=true" \
            --header "Authorization: Bearer $gitlab_api_token" \
            --header 'Content-Type: application/json' \
            --data @-)"

    [[ "${verbose}" == 'true' ]] && echo -e "Merged YAML:\\n$url/projects/$ci_project_path/repository/files/$file_path?ref=$ref\\n"
    if [[ "$(yq ".valid" <<< "${response}")" == 'true' ]]; then
        yq -r '.merged_yaml' <<< "$response"
    elif [[ "$(yq ".valid" <<< "${response}")" == 'false' ]]; then
        echo -e "[ERROR] $(jq -r '.errors[]' <<< "$response")"
        exit 1
    else
        echo -e "[ERROR] $(jq -r '.message' <<< "$response")"
        exit 1
    fi
}

parse_args(){
    if [[ $# -ne 0 ]]; then
        while [[ $# -ne 0 ]]; do
            arg_vars "${@}"
            shift
        done
    fi
    gather_missing_args
    url+="/api/v4"
    encode_args
}

url_encode(){
    printf %s "${1}" | jq -sRr @uri
}

main "$@"
