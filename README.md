# gl-ci-merge

Command line tool utilizing the Gitlab CI Lint API to merge all included templates in a ci file. Useful for viewing and testing pipeline changes before pushing commits to a remote Gitlab project or verifying all recursively included pipeline objects.

## Usage

By default, calling this CLI tool with no arguments will make assumptions based on your current directory. It will use `git` to check for the Gitlab server URL, project path, file, and also check for an API token set as an environment variable. Passing any/all of these args will take priority over the defaults.

Pass the `--help` or `-h` arg. to get view the help menu

`gi-ci-merge --help`

```
Help Menu
    -f|--file       File or template to merge, defaults to ".gitlab-ci.yml"
    -h|--help       Display (this) help menu
    -r|--ref        git tag or branch
    -p|--project    project path e.g. "group/subgroup/project"
    -u|--url        API domain
    -t|--token      Gitlab api token or GITLAB_TOKEN env var must be specified
    -v|--verbose    pass this arg to print 'Merged YAML' banner

Note: The --project and --url args are obtained automatically from git config
      in the current working directory if either are not specified.
```

## Requirements
- bash
- curl
- git
- jq
- python>=3.6
- python-yq

## Installation

### Mac
```
brew install jq python@3.9
pip install yq
```

## License
This project is licensed under the Apache License 2.0. [learn more](http://choosealicense.com/licenses/apache-2.0/)
